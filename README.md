# Doxygen CMake Sphinx

Example project using Sphinx to generate HTML Documentation for CMake Modules (.cmake files).

Original location of this project: https://gitlab.com/Pro1/doxygen-cmake-sphinx

Based on https://github.com/smrfeld/cpp_doxygen_sphinx and extended to show the usage of Sphinx to generate documentation for
custom CMake Modules using sphinx.

CMake support for Sphinx is added via the [Sphinx Domain for Modern CMake](https://pypi.org/project/sphinxcontrib-moderncmakedomain/)
which is based on the same Sphinx settings as CMake is using for its own documentation.

The Gitlab CI Pipeline is automatically running sphinx.
The resulting output can be found here: https://pro1.gitlab.io/doxygen-cmake-sphinx/

If you fork this project, the location of your own Gitlab Page will be shown in Repo -> Settings -> Pages after the first CI run.

## How to build the documentation

Use the following commands to build the Documentation for the Source Code and CMake Modules:

```bash
mkdir build
cd build
cmake ..
make documentation
```

The output is stored in: `build/doc/html`

## How to use CMake Documentation

Check out the [HelloWorld.cmake](./cmake/HelloWorld.cmake) file to see an example documentation.

For usage explanation of possible `:cmake:` directives, refer to: https://pypi.org/project/sphinxcontrib-moderncmakedomain/

The corresponding Sphinx include is shown in [doc/cmake/index.rst](./doc/cmake/index.rst) and referenced files.